--
-- PostgreSQL database dump
--

-- Dumped from database version 10.4
-- Dumped by pg_dump version 11.5

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'SQL_ASCII';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: sample_dev; Type: SCHEMA; Schema: -; Owner: -
--

CREATE SCHEMA sample_dev;


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: drink; Type: TABLE; Schema: sample_dev; Owner: -
--

CREATE TABLE sample_dev.drink (
    name character varying(255) NOT NULL
);


--
-- Name: drink_humans; Type: TABLE; Schema: sample_dev; Owner: -
--

CREATE TABLE sample_dev.drink_humans (
    drinks_name character varying(255) NOT NULL,
    humans_name character varying(255) NOT NULL
);


--
-- Name: human; Type: TABLE; Schema: sample_dev; Owner: -
--

CREATE TABLE sample_dev.human (
    name character varying(255) NOT NULL,
    address character varying(255)
);


--
-- Data for Name: drink; Type: TABLE DATA; Schema: sample_dev; Owner: -
--

COPY sample_dev.drink (name) FROM stdin;
\.


--
-- Data for Name: drink_humans; Type: TABLE DATA; Schema: sample_dev; Owner: -
--

COPY sample_dev.drink_humans (drinks_name, humans_name) FROM stdin;
\.


--
-- Data for Name: human; Type: TABLE DATA; Schema: sample_dev; Owner: -
--

COPY sample_dev.human (name, address) FROM stdin;
\.


--
-- Name: drink_humans drink_humans_pkey; Type: CONSTRAINT; Schema: sample_dev; Owner: -
--

ALTER TABLE ONLY sample_dev.drink_humans
    ADD CONSTRAINT drink_humans_pkey PRIMARY KEY (drinks_name, humans_name);


--
-- Name: drink drink_pkey; Type: CONSTRAINT; Schema: sample_dev; Owner: -
--

ALTER TABLE ONLY sample_dev.drink
    ADD CONSTRAINT drink_pkey PRIMARY KEY (name);


--
-- Name: human human_pkey; Type: CONSTRAINT; Schema: sample_dev; Owner: -
--

ALTER TABLE ONLY sample_dev.human
    ADD CONSTRAINT human_pkey PRIMARY KEY (name);


--
-- Name: drink_humans fk6hod1xn107ivpija3jh4qh4e9; Type: FK CONSTRAINT; Schema: sample_dev; Owner: -
--

ALTER TABLE ONLY sample_dev.drink_humans
    ADD CONSTRAINT fk6hod1xn107ivpija3jh4qh4e9 FOREIGN KEY (humans_name) REFERENCES sample_dev.human(name);


--
-- Name: drink_humans fkjtl9fnpjwt3pleld499qpsdvc; Type: FK CONSTRAINT; Schema: sample_dev; Owner: -
--

ALTER TABLE ONLY sample_dev.drink_humans
    ADD CONSTRAINT fkjtl9fnpjwt3pleld499qpsdvc FOREIGN KEY (drinks_name) REFERENCES sample_dev.drink(name);


--
-- PostgreSQL database dump complete
--

